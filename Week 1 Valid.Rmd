---
title: "Week 1 Assessment"
author: "Vishal Bandari"
output: html_document
---

</hr>

#Question 1 Solution

###Visual 1

<h4><b>Source             :</b></h4> Gramener (https://gramener.com/demo/#education)

<h4><b>URL                :</b></h4> http://bschools.businesstoday.in/

<h4><b>Classification     :</b></h4>

<b><i>Exploratory vs Explanatory - Exploratory</i></b>
  
  <p><b>Reason</b>: It is Exploratory visual becuase the visual is interactive and helps us explore the best college under any given category such as Learning Experience, Living Experience, Selection Process and Establishment, Future Orientation, Placement Performance. We can just drag on any of the category to get the best combination with certain Rank in Category.</p>
  

<b><i>Data Visualization vs Infographics - Data Visualization</b></i>

  <p><b>Reason</b>: It is Data Visual becuase it is Data Driven, Visually Informative and can be easily refreshed with new Data.</p>


###Visual 2

<h4><b>Source             :</b></h4> FLOWINGDATA (http://flowingdata.com/)

<h4><b>URL                :</b></h4> http://flowingdata.com/2018/07/09/graphics-explaining-thai-boys-rescue/

<h4><b>Classification     :</b></h4>

<b><i>Data Visualization vs Infographics - Data Visualization</b></i>

  <p><b>Reason</b>: It is Infograpgic Visual because the visual is story driven and less reproducible.</p>


###Visual 3

<h4><b>Source             :</b></h4> Gramener (https://gramener.com/demo/#agriculture)

<h4><b>URL                :</b></h4> https://gramener.com/rra/Templates/District_Map/index.html

<h4><b>Classification     :</b></h4>

<b><i>Exploratory vs Explanatory - Exploratory</i></b>
  
  <p><b>Reason</b>: It is Exploratory visual becuase the visual is interactive and helps us explore the pattern for the selected metrics such as Cropping Intensity %, Net Irrigated Area, Gross Irrigated Area etc. in Rainfed, Irrigated and Both Areas. We can select any metric, type of the area and also define color scale for a appealing Visual</p>
  

<b><i>Data Visualization vs Infographics - Data Visualization</b></i>

  <p><b>Reason</b>: It is Data Visual becuase it is Data Driven, Visually Informative and can be easily refreshed with new Data.</p>

</hr>

#Question 2 Solution

```{r}
US_Car_Accidents = read.csv("us.car.accidents.csv")
str(US_Car_Accidents)
```

#Questipn 3 Solution

##Question 3 a.
```{r}
valid_Races =US_Car_Accidents[US_Car_Accidents$Race != '' & US_Car_Accidents$Race != '\\N',]
number_Of_Different_Types_Of_Races = length(unique(valid_Races$Race))
print(number_Of_Different_Types_Of_Races)
different_Types_Of_Races           = unique(valid_Races$Race)
print(different_Types_Of_Races)
```

##Question 3 b.
```{r}
frequency_Of_Different_Race_Accidents = table(US_Car_Accidents$Race, exclude = c(NA,NULL,"","\\N"))
print("Frequency Of Different Races:")
print(frequency_Of_Different_Race_Accidents)
```

##Question 3 c
```{r}
table(US_Car_Accidents$Injury.Severity) / length(US_Car_Accidents$Injury.Severity) *100

```

#Question 4
```{r}
Col_Names <- c('State','Age','Atmospheric.Condition','Crash.Date','Race','Roadway','Gender','Drug.Involvement','Injury.Severity');
Uniques_Levels <- c();
Most_Frequent <- c();
Percentage <- c();
Percentage_Column_Contirbuting_80_Percentage <- c();

for (col_Name in Col_Names) {
  valid_Values_DataSet <- US_Car_Accidents[US_Car_Accidents[[col_Name]] != '' & US_Car_Accidents[[col_Name]] != '\\N',] 
  number_Of_Unique_Levels_Current_Column = length(unique(valid_Values_DataSet[[col_Name]]));
  number_Of_Valid_Records <- length(valid_Values_DataSet[[col_Name]]);
  
  Uniques_Levels <- c(Uniques_Levels,number_Of_Unique_Levels_Current_Column);
  
  Most_Frequent <- c(Most_Frequent,rownames(sort(table(valid_Values_DataSet[[col_Name]]),decreasing = TRUE))[1]);
  
  Percentage <- c(Percentage,(sort(table(valid_Values_DataSet[[col_Name]]) / number_Of_Valid_Records*100,decreasing= TRUE))[1]);
  
  rowCount <- 0;
  percentageCount <- 0;
  while(percentageCount < 80)
  {
    rowCount <- rowCount+1;
    percentageCount <- percentageCount + sort(table(valid_Values_DataSet[[col_Name]]) / number_Of_Valid_Records*100,decreasing= TRUE)[rowCount];
  }
  Percentage_Column_Contirbuting_80_Percentage <- c(Percentage_Column_Contirbuting_80_Percentage,rowCount / number_Of_Unique_Levels_Current_Column*100);
}

print(data.frame(Col_Names,Uniques_Levels,Most_Frequent,Percentage,Percentage_Column_Contirbuting_80_Percentage))
```



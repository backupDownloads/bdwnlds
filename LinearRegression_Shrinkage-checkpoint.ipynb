{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Shrinkage methods for Linear Regression\n",
    "\n",
    "In this example, we will look at shrinkage methods for linear regression models, which help determine which features are the most important for the prediction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.linear_model import LinearRegression, Ridge, Lasso\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "from sklearn import datasets\n",
    "from sklearn.metrics import accuracy_score"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start by loading the dataset. For this example, we will use the Breast Cancer Wisconsin dataset - details can be found at https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Prognostic%29. \n",
    "\n",
    "Remember to standardize the inputs before using Ridge Regression!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(569, 30)\n",
      "[[ 1.09706398 -2.07333501  1.26993369 ...  2.29607613  2.75062224\n",
      "   1.93701461]\n",
      " [ 1.82982061 -0.35363241  1.68595471 ...  1.0870843  -0.24388967\n",
      "   0.28118999]\n",
      " [ 1.57988811  0.45618695  1.56650313 ...  1.95500035  1.152255\n",
      "   0.20139121]\n",
      " ...\n",
      " [ 0.70228425  2.0455738   0.67267578 ...  0.41406869 -1.10454895\n",
      "  -0.31840916]\n",
      " [ 1.83834103  2.33645719  1.98252415 ...  2.28998549  1.91908301\n",
      "   2.21963528]\n",
      " [-1.80840125  1.22179204 -1.81438851 ... -1.74506282 -0.04813821\n",
      "  -0.75120669]]\n"
     ]
    }
   ],
   "source": [
    "dataset = datasets.load_breast_cancer()\n",
    "X = dataset.data\n",
    "print(X.shape)\n",
    "y = dataset.target\n",
    "features = dataset.feature_names\n",
    "\n",
    "# Standardize the inputs - zero mean and unit variance\n",
    "\n",
    "scaler = StandardScaler()\n",
    "X = scaler.fit_transform(X)\n",
    "print(X)\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's look at the coefficients returned during Linear Regression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = LinearRegression()\n",
    "model.fit(X_train, y_train)\n",
    "beta = model.coef_\n",
    "\n",
    "# Plot the coefficients\n",
    "xaxis = np.arange(beta.shape[0])\n",
    "plt.figure(figsize=(10,5))\n",
    "plt.bar(xaxis, beta)\n",
    "plt.title('Linear Regression Coefficients')\n",
    "plt.xlabel('Attribute')\n",
    "plt.ylabel('Coefficient Value')\n",
    "plt.axhline(0, color='black')\n",
    "plt.xticks(xaxis, features, rotation='vertical')\n",
    "plt.ylim(-1.5,1.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us fit this data using Ridge Regression, and observe the model parameters.\n",
    "\n",
    "$\\hat{\\beta}_{ridge} = argmin\\{ \\sum_{i=1}^n(y_i - \\beta_0 - \\sum_{j=1}^px_{ij}\\beta_j)^2 + \\lambda \\sum_{j=1}^p \\beta_j^2 \\}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Ridge(alpha=0.5)\n",
    "model.fit(X_train, y_train)\n",
    "beta = model.coef_\n",
    "\n",
    "# Plot the coefficients\n",
    "xaxis = np.arange(beta.shape[0])\n",
    "plt.figure(figsize=(10,5))\n",
    "plt.bar(xaxis, beta)\n",
    "plt.title('Ridge Regression Coefficients')\n",
    "plt.xlabel('Attribute')\n",
    "plt.ylabel('Coefficient Value')\n",
    "plt.axhline(0, color='black')\n",
    "plt.xticks(xaxis, features, rotation='vertical')\n",
    "plt.ylim(-1.5,1.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that several of the coefficients have shrunk to low values close to zero, and there are fewer attributes that dominate.\n",
    "\n",
    "Now let us look at LASSO, a shrinkage method that enforces sparsity.\n",
    "\n",
    "$\\hat{\\beta}_{lasso} = argmin\\{ \\sum_{i=1}^n(y_i - \\beta_0 - \\sum_{j=1}^px_{ij}\\beta_j)^2 + \\lambda \\sum_{j=1}^p |\\beta_j| \\}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Lasso(alpha=0.01)\n",
    "model.fit(X_train, y_train)\n",
    "beta = model.coef_\n",
    "\n",
    "# Plot the coefficients\n",
    "xaxis = np.arange(beta.shape[0])\n",
    "plt.figure(figsize=(10,5))\n",
    "plt.bar(xaxis, beta)\n",
    "plt.title('LASSO Coefficients')\n",
    "plt.xlabel('Attribute')\n",
    "plt.ylabel('Coefficient Value')\n",
    "plt.axhline(0, color='black')\n",
    "plt.xticks(xaxis, features, rotation='vertical')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

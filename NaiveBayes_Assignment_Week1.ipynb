{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "# Naive Bayes with categorical data\n",
    "\n",
    "We previously learned how to use the Naive Bayes classifier on the Iris dataset, where each attribute had continuous values. Let us now try classifying a dataset with discrete attributes. For this task, we will use the Mushroom dataset, and build a classifier that tells us whether a given mushroom is edible or poisonous. To understand the dataset and its attributes, read the description at https://archive.ics.uci.edu/ml/datasets/Mushroom.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn import metrics\n",
    "from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB\n",
    "from sklearn.preprocessing import MultiLabelBinarizer\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(5686, 22)\n",
      "(2438, 22)\n"
     ]
    }
   ],
   "source": [
    "data = pd.read_csv('agaricus-lepiota.data', sep=\",\", header=None)\n",
    "\n",
    "# The first column of the dataset gives the label - poisonous or edible\n",
    "X = data.values[:,1:]\n",
    "y = data.values[:,0]\n",
    "# Convert binary class labels ('p' and 'e') to integers (0 and 1)\n",
    "y = (y == 'e').astype(int)\n",
    "\n",
    "num_attributes = X.shape[1]\n",
    "\n",
    "# Split train and test data\n",
    "X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.3,random_state=5)\n",
    "print(X_train.shape)\n",
    "print(X_test.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "As we saw before, when the events model is discrete, we need to use the frequency of each feature and class to estimate the likelihood and probabilities.\n",
    "\n",
    "For a given categorical feature, we count and find the frequency of each possible value of the feature in the given datapoint. For a text classification problem, this would correspond to a bag-of words approach. In this problem, since a given feature can only have one value, we can simply take a one-hot encoding of the feature. For example, take the *cap-surface* feature for a few datapoints,\n",
    "\n",
    "| cap-surface |\n",
    "| ----------- |\n",
    "| f           |\n",
    "| g           |\n",
    "| y           |\n",
    "| s           |\n",
    "| y           |\n",
    "\n",
    "On one-hot encoding, we would get the following.\n",
    "\n",
    "| f    | g    | y    | s    |\n",
    "| ---- | ---- | ---- | ---- |\n",
    "| 1    | 0    | 0    | 0    |\n",
    "| 0    | 1    | 0    | 0    |\n",
    "| 0    | 0    | 1    | 0    |\n",
    "| 0    | 0    | 0    | 1    |\n",
    "| 0    | 0    | 1    | 0    |\n",
    "\n",
    "This can be done for each individual feature. \n",
    "\n",
    "We know that the Naive Bayes model assumes the features are all conditionally independent of each other.\n",
    "\n",
    "$$P(X_1,X_2,...X_n | C) = P(X_1|C)P(X_2|C)...P(X_n|C)$$\n",
    "\n",
    "Therefore, we can simply fit a Multinomial distribution for each feature, encoded as given above, and then multiply the probabilities for all the features.\n",
    "\n",
    "We will implement this in the function below. For each attribute, make _dummy variables_ by taking a one-hot encoding for each possible value of the attribute. You can do this with numpy directly, or use the MultiLabelBinarizer() function in scikit-learn. Compute the class-conditional probabilities for each attribute using Multinomial Naive Bayes, and then multiply the probabilities for all the classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def NaiveBayesCategorical(X_train, X_test, y_train):\n",
    "    probs = []\n",
    "    for i in range(num_attributes):\n",
    "        ### BEGIN SOLUTION\n",
    "        # Convert each categorical feature into an array of 'counts'\n",
    "        mlb = MultiLabelBinarizer()\n",
    "        X_train_counts = mlb.fit_transform(X_train[:,i])\n",
    "        X_test_counts = mlb.transform(X_test[:,i])\n",
    "        model_i = MultinomialNB()\n",
    "        model_i.fit(X_train_counts, y_train)\n",
    "        probs.append(model_i.predict_proba(X_test_counts))\n",
    "        ### END SOLUTION\n",
    "    # Get final probability by multiplying probabilities of individual attributes (by the Naive Bayes assumption!)\n",
    "    test_probs = np.prod(np.array(probs),axis=0)\n",
    "    return test_probs\n",
    "\n",
    "test_probs = NaiveBayesCategorical(X_train, X_test, y_train)\n",
    "predictions = np.argmax(test_probs, axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "## Evaluating a classifier\n",
    "\n",
    "After performing classification, typically we would measure the prediction accuracy on the test set, that is, the percentage of data points correctly labeled by the classifier. It tells us how often our model is correct overall."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9376538146021329\n"
     ]
    }
   ],
   "source": [
    "# Evaluate accuracy\n",
    "\n",
    "acc = metrics.accuracy_score(y_test,predictions)\n",
    "print(acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "There are other ways to evaluate the classifier that can shine light on the nature of its performance.\n",
    "\n",
    "### Confusion Matrix\n",
    "\n",
    "The confusion matrix gives us the following - \n",
    "\n",
    "**True Positives (TP)** - How many positive examples were classified correctly?\n",
    "\n",
    "**True Negatives (TN)** - How many negative examples were classified correctly?\n",
    "\n",
    "**False Positives (FP)** - How many negative examples were classified positive?\n",
    "\n",
    "**False Negatives (FN)** - How many positive examples were classified negative?\n",
    "\n",
    "\n",
    "|           .            | Predicted Negative(0) | Predicted Positive(1) |\n",
    "|------------------------|-----------------------|-----------------------|\n",
    "| **Actual Negative(0)** | true negative         | false positive        |\n",
    "| **Actual Positive(1)** | false negative        | true positive         |\n",
    "\n",
    "\n",
    "We can rewrite accuracy as the ratio of true positives and negatives to the total number of examples.\n",
    "\n",
    "$$ Accuracy = \\frac{TP + TN}{\\text{total examples}} $$\n",
    "\n",
    "### Precision\n",
    "\n",
    "Precision tells us how often the model is correct when it predicts positive. It is useful when the costs of false positives are high. Think about a model that detects whether a patient has cancer. Would it be acceptable to falsely diagnose more patients with cancer?\n",
    "\n",
    "$$ Precision = \\frac{TP}{TP + FP} $$\n",
    "\n",
    "### Recall\n",
    "\n",
    "Recall tells us how often the model is correct when it predicts negative. It is useful when the costs of false negatives are high. Think about a model that detects whether a patient has a highly contagious disease. If the model let more infected patients pass with a positive diagnosis, we would be letting infected people out into the world to spread disease.\n",
    "\n",
    "$$ Recall = \\frac{TP}{TP + FN} $$\n",
    "\n",
    "### F1 Score\n",
    "\n",
    "This is the harmonic average of the precision and recall. An F1 score of 1 implies perfect precision and recall.\n",
    "\n",
    "$$ F_1 = 2 \\times \\frac{precision \\times recall}{precision + recall} $$\n",
    "\n",
    "### Micro average\n",
    "\n",
    "In the micro-average method, the true positives, false positives, and false negatives are summed up for all classes. The precision, recall and F1 can then be computed using these.\n",
    "\n",
    "### Macro average\n",
    "\n",
    "In the macro-average method, the true positives, false positives, and false negatives are summed up for each class separately. The precision, recall and F1 are computed for each class, and then averaged.\n",
    "\n",
    "### Weighted average\n",
    "\n",
    "This is a modification to the macro average method that takes class imbalance into account. As above, the precision, recall and F1 are computed for each class, but the average is weighted by the number of true instances per class.\n",
    "\n",
    "### Support\n",
    "\n",
    "This is the number of true instances for a given class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "              precision    recall  f1-score   support\n",
      "\n",
      "           0       1.00      0.87      0.93      1206\n",
      "           1       0.89      1.00      0.94      1232\n",
      "\n",
      "   micro avg       0.94      0.94      0.94      2438\n",
      "   macro avg       0.95      0.94      0.94      2438\n",
      "weighted avg       0.94      0.94      0.94      2438\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Print classification report\n",
    "\n",
    "classification_report = metrics.classification_report(y_test,predictions)\n",
    "print(classification_report)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Confusion matrix:\n",
      "[[1054  152]\n",
      " [   0 1232]]\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXQAAAEWCAYAAAB2X2wCAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvOIA7rQAAHipJREFUeJzt3XecVNX5x/HPd2miIFJ0KYIVjJpE7AmJxm6smFgilqg/EqyJJRqNGtSoiYk1JjZUDGqCGCOJLZYQE3sBJIgNUVGpIiogTVie3x/3Lo64ZXaZ2Zm9+32/Xve1M+feuee5yuuZM8+ce0YRgZmZNX8VpQ7AzMwKwwndzCwjnNDNzDLCCd3MLCOc0M3MMsIJ3cwsI5zQbbVJai/pfknzJP11Nc5zpKRHCxlbqUjaSdIbpY7DWhZ5HnrLIekI4AzgK8ACYAJwaUQ8tZrnPRr4CTAgIpavdqBlTlIAfSNiSqljMcvlEXoLIekM4Brg10Al0Ae4HhhYgNNvAExuCck8H5JalzoGa6EiwlvGN6AT8ClwaB3HtCNJ+DPS7RqgXbpvF2Aa8DPgA2AmcFy67yLgM2BZ2sdg4ELgzpxzbwgE0Dp9fizwNsmnhHeAI3Pan8p53QDgRWBe+ndAzr7/ABcDT6fneRToVsu1Vcf/85z4DwL2BSYDHwHn5hy/A/As8El67B+Btum+J9JrWZhe7w9yzn82MAu4o7otfc0maR/bpM97AnOAXUr9b8NbtjaP0FuGbwJrAKPrOOY84BtAf2ArkqR2fs7+7iRvDL1IkvZ1kjpHxAUko/5REdEhIm6tKxBJawHXAvtEREeSpD2hhuO6AA+mx3YFrgIelNQ157AjgOOA9YC2wJl1dN2d5L9BL2AocDNwFLAtsBPwS0kbpcdWAacD3Uj+2+0OnAQQETunx2yVXu+onPN3Ifm0MiS344h4iyTZ3ylpTeA2YERE/KeOeM0azAm9ZegKfBh1l0SOBH4VER9ExBySkffROfuXpfuXRcRDJKPTzRoZzwrgq5LaR8TMiHilhmP2A96MiDsiYnlEjAReBw7IOea2iJgcEYuBu0nejGqzjOT7gmXAXSTJ+vcRsSDt/1WSNzIiYlxEPJf2OxW4CfhOHtd0QUQsTeP5goi4GZgCPA/0IHkDNSsoJ/SWYS7QrZ7abk/g3Zzn76ZtK8+xyhvCIqBDQwOJiIUkZYoTgJmSHpT0lTziqY6pV87zWQ2IZ25EVKWPqxPu7Jz9i6tfL6mfpAckzZI0n+QTSLc6zg0wJyKW1HPMzcBXgT9ExNJ6jjVrMCf0luFZYClJ3bg2M0jKBdX6pG2NsRBYM+d599ydEfFIROxJMlJ9nSTR1RdPdUzTGxlTQ9xAElffiFgbOBdQPa+pc7qYpA4k30vcClyYlpTMCsoJvQWIiHkkdePrJB0kaU1JbSTtI+l36WEjgfMlrSupW3r8nY3scgKws6Q+kjoBv6jeIalS0sC0lr6UpHSzooZzPAT0k3SEpNaSfgBsATzQyJgaoiMwH/g0/fRw4ir7ZwMbN/CcvwfGRsSPSL4buHG1ozRbhRN6CxERV5LMQT+fZIbF+8ApwN/TQy4BxgITgZeB8WlbY/p6DBiVnmscX0zCFWkcM0hmfnyHLydMImIusD/JzJq5JDNU9o+IDxsTUwOdSfKF6wKSTw+jVtl/ITBC0ieSDqvvZJIGAt/l8+s8A9hG0pEFi9gM31hkZpYZHqGbmWWEE7qZWUY4oZuZZYQTuplZRpTtIkL9Btzob2vtS557vFf9B1mL06XdAfXdJ1Cv9n0G5Z1zFr83crX7KwaP0M3MMqJsR+hmZk1Jav7jWyd0MzOgIgPL2Df/KzAzKwCP0M3MMkIqy+85G8QJ3cwMyMIcESd0MzNccjEzywwndDOzjPAsFzOzjPAI3cwsI5zQzcwyQvX+bGz5c0I3M8MjdDOzzKioaP7psPlfgZlZQXiEbmaWCS65mJllhBO6mVlGyCUXM7Ns8AjdzCwjKipalTqE1eaEbmaGSy5mZpnhkouZWUY4oZuZZYRLLmZmGSHf+m9mlg3+kWgzs4xwycXMLCP8paiZWVa45GJmlhHNf4DuhG5mBkBF88/oTuhmZuARuplZVoRr6GZmGdH887kTupkZABXNP6M7oZuZgactmpllRqvmn9Az8L2umVkBSPlv9Z5KwyV9IGlSTlsXSY9JejP92zltl6RrJU2RNFHSNjmvOSY9/k1Jx9TXrxO6mRkkX4rmu9XvT8B3V2k7BxgTEX2BMelzgH2Avuk2BLgBkjcA4AJgR2AH4ILqN4HaOKGbmUHypWi+Wz0i4gngo1WaBwIj0scjgINy2m+PxHPAOpJ6AHsDj0XERxHxMfAYX36T+OIl5H2xZmZZ1oARuqQhksbmbEPy6KEyImamj2cBlenjXsD7OcdNS9tqa6+VvxQ1MwOiVf7j24gYBgxrdF8RISka+/raeIRuZgaFrqHXZHZaSiH9+0HaPh3onXPc+mlbbe21ckI3M4OCznKpxX1A9UyVY4B/5LT/MJ3t8g1gXlqaeQTYS1Ln9MvQvdK2WrnkYmYGBb1TVNJIYBegm6RpJLNVLgPuljQYeBc4LD38IWBfYAqwCDgOICI+knQx8GJ63K8iYtUvWr/ACd3MDAq6lktEDKpl1+41HBvAybWcZzgwPN9+ndDNzMC3/puZZUYGbv13QjczA4/Qzcwyo/nncyf0Uvn1ubuw67c2YO7Hi9n/qLsB6NSxHddcvCe9enRk+swFnPrLR5m/4DN22LonN/x2b6bNWADAo/99h+tuG7fyXBUV4t7hBzN7zkKOP+ufJbkeK7xLho7imf++SucuHfjz6LMAuOX6R/jHvc/TuXMHAE746T4M2GlzXnh2Mtdf8yDLllXRpk0rTjljf7bbsW8pw292wuuhW2Pd+9Ab3HnPJH43dLeVbUOO3ppnx01j2B0TGHJ0f4YcvTVXXP88AGP/N6vWZH3MYV/jrakf02Gttk0SuzWN/Q7cjkMP/xa/Om/kF9oPP2pnjjx2ly+0dVpnLS7/w/+x7nqdeOvNmZx24s3c/6+hTRhtBmSg5FK0G4skfUXS2emykNemjzcvVn/NzdgJM5k3f+kX2nbfaUNGPzQZgNEPTWaPnTaq9zyV667FLgP68Nf7XytKnFY6W2+3CWt3WjOvYzfbvBfrrtcJgI037c7SJcv47LPlxQwve4p/p2jRFSWhSzobuIvk0l9INwEjJZ1T12tbsm5d2jNn7iIA5sxdRLcu7Vfu6//VSu4bcQi3XLkvm270+Qqa5502gN9d9xwrVjR5uFYi99z1NEcdfCWXDB3F/PmLvrT/8ccmstnm69O2rT+AN0irivy3MlWsyAYD20fEZRFxZ7pdRrKm7+DaXpS7gtm82U8WKbTmI9Kle155Yw67fv9ODjzmHu64ZxLXX5asoLnLgD7M/XgJr7zxYQmjtKb0/R8M4J4Hf8Htfz2dbt3W5tor7v/C/renzOL6ax7i7KEHlyjCZswj9FqtAHrW0N4j3VejiBgWEdtFxHadKncqUmjl68OPFrNu1+Qj9rpd12Tux4sBWLhoGYsWJx+f//vse7RuXUHnTmuw7de7s/u3N+DffzuSq3+1B9/YtieXX7Bbree35q9L1460alVBRUUFAw/ekddefm/lvg9mfcI5p/+JX156OOv37lbCKJupAq6HXirF+kx2GjBG0pt8vp5vH2BT4JQi9dns/fupqXxv334Mu2MC39u3H2OenAokpZgPP0qS+9c3X48KwcfzlnDljS9w5Y0vALDD1j0ZfMRWnHXRv0sVvjWBD+fMp9u6awPwn39PYuO+PQBYMH8xPzvlVk46dT+22rr+716sBmWcqPNVlIQeEQ9L6kdSYqlekH068GJEVBWjz+bmqot2Z4ete9J5nTV44u9Hce0tYxl2x0v8/pI9OWT/zZkxawGnnv8YAN/ddWMGfW9LqqpWsGRpFacP/VeJo7emMPTndzJ+7Ft88slCDtzjYn500l68NPYtJr8+A0n06NmZs4ceAiR19Wnvfcjwmx5j+E3Jv5trbvwxXbp2LOUlNCvR/PM5iij4GusF0W/AjeUZmJXUc4/X+YMt1kJ1aXfAaqfjjY//W9455+2bDi7L9O+vwc3MwCUXM7PMKN/ZiHlzQjczg0zcKeqEbmYGLrmYmWVFeIRuZpYRrZ3QzcyywSN0M7OMcA3dzCwjmn8+d0I3MwP/YpGZWXY4oZuZZUQrJ3Qzs2zwLBczs4xwycXMLCOc0M3MssG3/puZZYW/FDUzywiXXMzMMsIJ3cwsI5p/PndCNzODjN/6L6lLXS+MiI8KH46ZWYlkfJbLOCCo+YNIABsXJSIzs1Io0CwXSZsBo3KaNgaGAusAPwbmpO3nRsRD6Wt+AQwGqoCfRsQjjem71oQeERs15oRmZs1RRUVhzhMRbwD9ASS1AqYDo4HjgKsj4orc4yVtARwObAn0BP4lqV9EVDW073ovQYmjJP0yfd5H0g4N7cjMrJxJ+W8NsDvwVkS8W8cxA4G7ImJpRLwDTAEalWPzeU+6HvgmcET6fAFwXWM6MzMrVw1J6JKGSBqbsw2p5bSHAyNznp8iaaKk4ZI6p229gPdzjpmWtjVYPgl9x4g4GVgCEBEfA20b05mZWbmSlPcWEcMiYrucbVgN52sLHAj8NW26AdiEpBwzE7iy0NeQz7TFZWkdKNIg1wVWFDoQM7NSKlQNPcc+wPiImA1Q/RdA0s3AA+nT6UDvnNetn7Y1WD6XcC1JQb9S0qXAU8CvG9OZmVm5UkX+W54GkVNukdQjZ9/3gEnp4/uAwyW1k7QR0Bd4oTHXUO8IPSL+LGkcSXEf4KCIeK0xnZmZlatCTkOXtBawJ3B8TvPvJPUnqXZMrd4XEa9Iuht4FVgOnNyYGS6Q/52iawLVZZf2jenIzKycFfJG0YhYCHRdpe3oOo6/FLh0dfvNZ9riUGAE0AXoBtwm6fzV7djMrJwUadpik8pnhH4ksFVELAGQdBkwAbikmIGZmTWlck7U+conoc8A1iCdtgi0o5HfwJqZlauKLP/AhaQ/kNTM5wGvSHosfb4njfwG1sysXGV9hD42/TuOZNpitf8ULRozsxLJdEKPiBFNGYiZWSllOqFXk9QX+A2wBUktHYCI8PK5ZpYZGfh9i7zuFL2NZA2C5cCuwO3AncUMysysqWVh2mI+Cb19RIwBFBHvRsSFwH7FDcvMrGlVtFLeW7nKZ9riUkkVwJuSTiGZstihuGGZmTWtch555yufEfqpJLf+/xTYFjgaOKaYQZmZNbUslFzyWZzrxfThpyQ/oWRmljnlnKjzVdeNRfeTroFek4g4sCgRmZmVQBZmudQ1Qr+ijn1mZplS0arUEay+um4s+m9TBmJmVkqZLrmYmbUkykBGd0I3M8MjdDOzzMh0Qi/1LJfJz+xWzNNbM9W+zwWlDsHK0OL3Dljtc2Q6oeNZLmbWgrTO5zbLMudZLmZmQIVqLUg0G14+18yMbNxY5OVzzcxIkmG+W7ny8rlmZiQll3y3cuXlc83MyEbJJZ+Enrt87sXAbnj5XDPLmNYtIaF7+VwzawlUxqWUfOUzy+VxarjBKCJ854+ZZUZLKbmcmfN4DeBgkhkvZmaZUc6zV/KVT8ll3CpNT0t6oUjxmJmVRDnPXslXPiWXLjlPK0h+V7RT0SIyMyuBFvGlKDCOpIYuklLLO8DgYgZlZtbUWkoNffOIWJLbIKldkeIxMyuJLJRc8vke4Jka2p4tdCBmZqVUofy3clXXeujdgV5Ae0lbk5RcANYmudHIzCwzsj7LZW/gWGB94Eo+T+jzgXOLG5aZWdMqZMlF0lRgAVAFLI+I7dIJJqOADYGpwGER8bGSHzP9PbAvsAg4NiLGN6bfutZDHwGMkHRwRPytMSc3M2suivADF7tGxIc5z88BxkTEZZLOSZ+fDewD9E23HUlWt92xMR3mcwnbSlqn+omkzpIuaUxnZmblqgmWzx0IjEgfjwAOymm/PRLPAetI6tGYDvKJbZ+I+KT6SUR8TPLRwMwsMwq8fG4Aj0oaJ2lI2lYZETPTx7OAyvRxL+D9nNdOS9saLJ9pi60ktYuIpQCS2gOetmhmmdKQ2Stpkh6S0zQsIoblPP92REyXtB7wmKTXc18fEaEirAaWT0L/MzBG0m3p8+NIfrXIzCwzGlJKSZP3sDr2T0//fiBpNLADMFtSj4iYmZZUPkgPnw70znn5+mlbg9V7DRHxW+ASYPN0uzhtMzPLjELNQ5e0lqSO1Y+BvYBJwH18/lsSxwD/SB/fB/xQiW8A83JKMw2SzwidiHgYeDgN8NuSrouIkxvToZlZOWpVUbAKSCUwOpmNSGvgLxHxsKQXgbslDQbeBQ5Lj3+I5HvJKSTTFhv9uxN5JfT0xqJBaQDvAPc2tkMzs3JUqFmLEfE2sFUN7XOB3WtoD6AgA+S67hTtR5LEBwEfkkyIV0TsWoiOzczKSRbWcqlrhP468CSwf0RMAZB0epNEZWbWxMp5jZZ81fUp4/vATOBxSTdL2p3Pb/83M8uUTC/OFRF/B/6efks7EDgNWE/SDcDoiHi0iWI0Myu6NhkoueQzbXFhRPwlIg4gmR/5Esn6A2ZmmZHpEXpN0tv+65xQb2bWHJVzos5XgxK6mVlWtXJCNzPLBo/QzcwyIuvz0M3MWow2HqGbmWWDSy5mZhnhkouZWUZ4louZWUa45GJmlhGtC7V+bgk5oZuZAa1cQzczy4YMDNCd0M3MwDV0M7PMcEI3M8sI19DNzDLCs1zMzDLCJRczs4zwnaJmZhnhtVys6J54YhyXXnozK1as4NBD92TIkENLHZIV0Y2XH88+u2/NnLnz2W7PnwPw63OPYN89tuGzZVW88+5shpx5I/PmL2K7rTbhj5f9CABJXHr1Pdz3yFjW79GFW64+ifXW7UQEDP/LGK4b/nApL6tZyEAJHUWU67vS5HINrMlUVVWx994ncNttF1NZ2ZVDDjmDq646i0037VPq0EqmfZ8LSh1CUX1rh6+wcNESbrn6pJUJffedvsZ/nnmFqqoVXPKLQQCc/5uRtF+jLZ8tW05V1Qq6r7cOzz98GRtvfxLrdl2b7uutw4RJU+mw1ho88+CvOezHV/L6m9NLeWlFtfi9katdMPn3jIfyzjm79dy3LAs0WXhTyqyJE99kgw160Lt3d9q2bcN+++3MmDHPlzosK6KnX3idjz759AttY558maqqFQC8MP5NenXvAsDiJZ+tbG/Xrg3VY7NZH3zChElTAfh04RJenzKdnulrrHZtKiLvrVy55FLGZs+eS/fu3VY+r6zsysSJk0sYkZXaD3+wC/fc/9zK59v334QbrziBPr26Mfi061Ym+Gp91u9G/y035MWXpjR1qM1OFma5NPkIXdJxdewbImmspLHDho1qyrDMyt7PTzmIquUruGv0UyvbXpzwFtvucRbfPuA8zjp5IO3atVm5b6012zHyptM566LbWfDp4lKE3KxUKP+tXJVihH4RcFtNOyJiGDAseeYaemVlV2bN+nDl89mz51JZ2bWEEVmpHHXIzuy7+9bsM+jSGve/MWUGny5cypab9Wb8xLdp3boVI286nVGjn+YfD7/YxNE2T1moPxcloUuaWNsuoLIYfWbR177Wl6lTZ/D++7OorOzKgw8+wZVXnlnqsKyJ7fmdrTjjxAPY69BfsXjJZyvbN+i9LtNmzKWqagV9enVjs0178u77cwC48fIhvDFlBtfe8lCpwm52VMYj73wVa4ReCewNfLxKu4BnitRn5rRu3YqhQ0/gRz+6gKqqFRx88B707btBqcOyIhrxh5+w0zc3p1vnjkx5/o9cfNU9SSmlbRse+PO5ALzw0hR+eu6tDNh+M848aSDLli1nxYrg1POGM/fjBQzYfjOOPHhnXn7tPZ77528AuOB3o3jk8QmlvLSyV86llHwVZdqipFuB2yLiqRr2/SUijqj/LC652JdlfdqiNU4hpi2O//DBvHPONt32K8v0X5QRekQMrmNfHsnczKxpyXeKmpllQ1kOuRvICd3MjGx8KZqFmTpmZqtNDdjqPI/UW9Ljkl6V9IqkU9P2CyVNlzQh3fbNec0vJE2R9IakvRt7DR6hm5lR0OVzlwM/i4jxkjoC4yQ9lu67OiKuyD1Y0hbA4cCWQE/gX5L6RURVQzv2CN3MjKTkku9Wl4iYGRHj08cLgNeAXnW8ZCBwV0QsjYh3gCnADo25Bid0MzMaVnLJXaYk3YbUeE5pQ2BroHpVvVMkTZQ0XFLntK0X8H7Oy6ZR9xtArZzQzcxoWEKPiGERsV3ONuxL55M6AH8DTouI+cANwCZAf2AmcGWhr8E1dDMzCnunqKQ2JMn8zxFxL0BEzM7ZfzPwQPp0OtA75+Xrp20N5hG6mRkFneUi4FbgtYi4Kqe9R85h3wMmpY/vAw6X1E7SRkBf4IXGXINH6GZmFPQ3Rb8FHA28LKl6AZ1zgUGS+gMBTAWOB4iIVyTdDbxKMkPm5MbMcAEndDMzoHA3FqVrWNV0tlqXvoyIS4Ga10ZuACd0MzOyUX92QjczIxu3/juhm5nhxbnMzDIjCz9w4YRuZoYTuplZZmQgnzuhm5mBf7HIzCwzPEI3M8sIT1s0M8uIVqUOoACc0M3M8AjdzCxDmn9Gd0I3MwPkhG5mlg1S81+eywndzAxwycXMLCOUgQV0ndDNzHDJxcwsQ1xyMTPLBM9yMTPLCCd0M7OMkJr/zf9O6GZmgGvoZmYZ4ZKLmVlmeNqimVkmeIRuZpYRysD6uU7oZmaAMvATF07oZmaAZ7mYmWWESy5mZpnhhG5mlglePtfMLDM8Qjczy4QKr4duZpYVTuhmZpmQhTtFm/9bkplZQagBWz1nkr4r6Q1JUySdU7SQV+ERupkZhZuHrmRh9euAPYFpwIuS7ouIVwvSQR2c0M3MKOit/zsAUyLibQBJdwEDgZac0Ps1/4JWgUgaEhHDSh1HOVj83shSh1A2/O+i0PLPOZKGAENymobl/L/oBbyfs28asOPqx1c/19CbhyH1H2ItkP9dlEhEDIuI7XK2snhjdUI3Myus6UDvnOfrp21F54RuZlZYLwJ9JW0kqS1wOHBfU3RcxjV0y1EWH+es7PjfRRmKiOWSTgEeAVoBwyPilaboWxHRFP2YmVmRueRiZpYRTuhmZhnhhF7mSnULsZUvScMlfSBpUqljsfLihF7Gcm4h3gfYAhgkaYvSRmVl4E/Ad0sdhJUfJ/TytvIW4oj4DKi+hdhasIh4Avio1HFY+XFCL2813ULcq0SxmFmZc0I3M8sIJ/TyVrJbiM2s+XFCL28lu4XYzJofJ/QyFhHLgepbiF8D7m6qW4itfEkaCTwLbCZpmqTBpY7JyoNv/TczywiP0M3MMsIJ3cwsI5zQzcwywgndzCwjnNDNzDLCCd3qJKlK0gRJkyT9VdKaq3GuXSQ9kD4+sK7VIyWtI+mkRvRxoaQz821f5Zg/STqkAX1t6BUPrZw4oVt9FkdE/4j4KvAZcELuTiUa/O8oIu6LiMvqOGQdoMEJ3awlc0K3hngS2DQdmb4h6XZgEtBb0l6SnpU0Ph3Jd4CV67m/Lmk88P3qE0k6VtIf08eVkkZL+l+6DQAuAzZJPx1cnh53lqQXJU2UdFHOuc6TNFnSU8Bm9V2EpB+n5/mfpL+t8qljD0lj0/Ptnx7fStLlOX0fv7r/Ic2KwQnd8iKpNcm67C+nTX2B6yNiS2AhcD6wR0RsA4wFzpC0BnAzcACwLdC9ltNfC/w3IrYCtgFeAc4B3ko/HZwlaa+0zx2A/sC2knaWtC3Jkgj9gX2B7fO4nHsjYvu0v9eA3DstN0z72A+4Mb2GwcC8iNg+Pf+PJW2URz9mTap1qQOwstde0oT08ZPArUBP4N2IeC5t/wbJD3A8LQmgLcmt6V8B3omINwEk3QkMqaGP3YAfAkREFTBPUudVjtkr3V5Kn3cgSfAdgdERsSjtI5+1br4q6RKSsk4HkqUVqt0dESuANyW9nV7DXsDXc+rrndK+J+fRl1mTcUK3+iyOiP65DWnSXpjbBDwWEYNWOe4Lr1tNAn4TETet0sdpjTjXn4CDIuJ/ko4FdsnZt+paGJH2/ZOIyE38SNqwEX2bFY1LLlYIzwHfkrQpgKS1JPUDXgc2lLRJetygWl4/BjgxfW0rSZ2ABSSj72qPAP+XU5vvJWk94AngIEntJXUkKe/UpyMwU1Ib4MhV9h0qqSKNeWPgjbTvE9PjkdRP0lp59GPWpDxCt9UWEXPSke5ISe3S5vMjYrKkIcCDkhaRlGw61nCKU4Fh6aqBVcCJEfGspKfTaYH/TOvomwPPpp8QPgWOiojxkkYB/wM+IFlyuD6/BJ4H5qR/c2N6D3gBWBs4ISKWSLqFpLY+Xknnc4CD8vuvY9Z0vNqimVlGuORiZpYRTuhmZhnhhG5mlhFO6GZmGeGEbmaWEU7oZmYZ4YRuZpYR/w/LId5PNXHg6QAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 2 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Obtain the confusion matrix\n",
    "confusion_matrix = metrics.confusion_matrix(y_test,predictions)\n",
    "print(\"Confusion matrix:\\n%s\" % confusion_matrix)\n",
    "\n",
    "# Prettier way to display the confusion matrix\n",
    "import seaborn as sns\n",
    "p = sns.heatmap(pd.DataFrame(confusion_matrix), annot=True, cmap=\"YlGnBu\" ,fmt='g')\n",
    "plt.title('Confusion matrix', y=1.1)\n",
    "plt.ylabel('Actual label')\n",
    "plt.xlabel('Predicted label')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "The above scores were generated by helpful Python libraries. Let us generate the scores ourselves, using the formulae given earlier, and see if they match."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy = 0.937654\n",
      "Precision = 0.890173\n",
      "Recall = 1.000000\n",
      "F1 score = 0.941896 \n",
      "\n",
      "Confusion matrix:\n",
      "[[1054  152]\n",
      " [   0 1232]]\n"
     ]
    }
   ],
   "source": [
    "def compute_confusion_matrix(y_test, predictions):\n",
    "\n",
    "    ### BEGIN SOLUTION\n",
    "    TP = 0\n",
    "    TN = 0\n",
    "    FP = 0\n",
    "    FN = 0\n",
    "    \n",
    "    num_examples = y_test.shape[0]\n",
    "\n",
    "    for i in range(num_examples):\n",
    "        if predictions[i] == 1:\n",
    "            if predictions[i] == y_test[i]: \n",
    "                TP += 1\n",
    "            else:\n",
    "                FP += 1\n",
    "        else:\n",
    "            if predictions[i] == y_test[i]:\n",
    "                TN += 1\n",
    "            else:\n",
    "                FN += 1\n",
    "                \n",
    "    accuracy = (TP + TN)/num_examples\n",
    "    precision = TP / (TP + FP)\n",
    "    recall = TP / (TP + FN)\n",
    "    f1 = 2 * (precision * recall) / (precision + recall)\n",
    "    confusion_matrix = np.array([[TN, FP],[FN, TP]])    \n",
    "    ### END SOLUTION    \n",
    "    \n",
    "    return accuracy, precision, recall, f1, confusion_matrix\n",
    "    \n",
    "accuracy, precision, recall, f1, confusion_matrix = compute_confusion_matrix(y_test, predictions)\n",
    "print(\"Accuracy = %f\" % accuracy)\n",
    "print(\"Precision = %f\" % precision)    \n",
    "print(\"Recall = %f\" % recall)    \n",
    "print(\"F1 score = %f \\n\" % f1)\n",
    "print(\"Confusion matrix:\\n%s\" % confusion_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "# Naive Bayes with mixed data (discrete and continuous)\n",
    "\n",
    "Now let's try using a dataset with both discrete and continuous attributes. For this task, we will use the Cleveland Heart Disease Dataset dataset from the UCI repository and perform binary classification. The classifier will tell us whether a given individual has heart disease or not. Before starting on this exercise, read the dataset description at https://archive.ics.uci.edu/ml/datasets/Heart+Disease and see the list of attributes. Identify which ones are discrete and which ones are continuous."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "# Refer to the dataset description or the table below to verify this\n",
    "categorical_indices = [1,2,5,6,8,10,11,12]\n",
    "continuous_indices = [0,3,4,7,9] "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(212, 13)\n",
      "(91, 13)\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "      <th>4</th>\n",
       "      <th>5</th>\n",
       "      <th>6</th>\n",
       "      <th>7</th>\n",
       "      <th>8</th>\n",
       "      <th>9</th>\n",
       "      <th>10</th>\n",
       "      <th>11</th>\n",
       "      <th>12</th>\n",
       "      <th>13</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>63.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>145.0</td>\n",
       "      <td>233.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>150.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2.3</td>\n",
       "      <td>3.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>67.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>160.0</td>\n",
       "      <td>286.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>108.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.5</td>\n",
       "      <td>2.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>67.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>120.0</td>\n",
       "      <td>229.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>129.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>2.6</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>7.0</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>37.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>130.0</td>\n",
       "      <td>250.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>187.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>3.5</td>\n",
       "      <td>3.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>41.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>130.0</td>\n",
       "      <td>204.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>172.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1.4</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "     0    1    2      3      4    5    6      7    8    9    10   11   12  13\n",
       "0  63.0  1.0  1.0  145.0  233.0  1.0  2.0  150.0  0.0  2.3  3.0  0.0  6.0   0\n",
       "1  67.0  1.0  4.0  160.0  286.0  0.0  2.0  108.0  1.0  1.5  2.0  3.0  3.0   2\n",
       "2  67.0  1.0  4.0  120.0  229.0  0.0  2.0  129.0  1.0  2.6  2.0  2.0  7.0   1\n",
       "3  37.0  1.0  3.0  130.0  250.0  0.0  0.0  187.0  0.0  3.5  3.0  0.0  3.0   0\n",
       "4  41.0  0.0  2.0  130.0  204.0  0.0  2.0  172.0  0.0  1.4  1.0  0.0  3.0   0"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data_train = pd.read_csv('cleveland.data', sep=\",\", header=None)\n",
    "\n",
    "X = data_train.values[:,:-1]\n",
    "y = data_train.values[:,-1]\n",
    "y = (y > 0).astype(int)\n",
    "\n",
    "X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.3,random_state=5)\n",
    "print(X_train.shape)\n",
    "print(X_test.shape)\n",
    "\n",
    "data_train.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "source": [
    "The data contains both categorical as well as continuous (numerical) features. Fortunately, since we are following the Naive Bayes assumption, we can easily incorporate both types of features in our classifier, by multiplying all the probabilities for each individual feature.\n",
    "\n",
    "For the categorical features, we follow the same method as before, by taking the one-hot encoding of each feature and fitting a Multinomial Naive Bayes model. The continuous features can be fit using Gaussian Naive Bayes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def NaiveBayesMixed(X_train, X_test, y_train):\n",
    "\n",
    "    probs = []   \n",
    "\n",
    "    # Fit categorical attributes\n",
    "    for i in categorical_indices:\n",
    "        \n",
    "        ### BEGIN SOLUTION\n",
    "        # Convert each categorical feature into an array of 'counts'\n",
    "        mlb = MultiLabelBinarizer()\n",
    "        X_train_counts = mlb.fit_transform(X_train[:,i].astype(str))\n",
    "        X_test_counts = mlb.transform(X_test[:,i].astype(str))\n",
    "\n",
    "        model_i = MultinomialNB()\n",
    "        model_i.fit(X_train_counts, y_train)\n",
    "        probs.append(model_i.predict_proba(X_test_counts))\n",
    "        ### END SOLUTION\n",
    "\n",
    "    # Fit continuous attributes\n",
    "    for i in continuous_indices:\n",
    "\n",
    "        ### BEGIN SOLUTION\n",
    "        X_train_attributes = X_train[:,i].reshape(-1, 1).astype(float)\n",
    "        X_test_attributes = X_test[:,i].reshape(-1, 1).astype(float)\n",
    "        model_continuous = GaussianNB()\n",
    "        model_continuous.fit(X_train_attributes, y_train) \n",
    "        probs.append(model_continuous.predict_proba(X_test_attributes))\n",
    "        ### END SOLUTION\n",
    "\n",
    "# Get final probability by multiplying probabilities of individual classifiers (by the Naive Bayes assumption!)\n",
    "    test_probs = np.prod(np.array(probs),axis=0)\n",
    "    return test_probs\n",
    "\n",
    "test_probs = NaiveBayesMixed(X_train, X_test, y_train)\n",
    "predictions = np.argmax(test_probs, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "locked": true,
     "solution": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy = 0.868132\n",
      "Precision = 0.933333\n",
      "Recall = 0.736842\n",
      "F1 score = 0.823529 \n",
      "\n",
      "Confusion matrix:\n",
      "[[51  2]\n",
      " [10 28]]\n"
     ]
    }
   ],
   "source": [
    "accuracy, precision, recall, f1, confusion_matrix = compute_confusion_matrix(y_test, predictions)\n",
    "print(\"Accuracy = %f\" % accuracy)\n",
    "print(\"Precision = %f\" % precision)    \n",
    "print(\"Recall = %f\" % recall)    \n",
    "print(\"F1 score = %f \\n\" % f1)\n",
    "print(\"Confusion matrix:\\n%s\" % confusion_matrix)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

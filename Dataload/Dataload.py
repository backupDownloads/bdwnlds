# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 23:19:49 2019

@author: vibandari
"""

import pandas as pd
import numpy as np
def unique(list1): 

    x = np.array(list1) 
    return np.unique(x) 
        
ref_file = 'Mapping.xlsx'
reference_DF = pd.read_excel(ref_file, sheet_name=0)
content_file = 'LibraryAttribute.xlsx'
content_DF = pd.read_excel(content_file, sheet_name=0)
for index, row in reference_DF.iterrows(): 
    row[1] = row[1].replace('LEL-','LELx-')
    content_DF['AV_LINK_VALUES__C'] = content_DF['AV_LINK_VALUES__C'].str.replace(row[0],row[1])
content_DF['AV_LINK_VALUES__C'] = content_DF['AV_LINK_VALUES__C'].str.replace('LELx-','LEL-')
content_DF.to_excel("output.xlsx",index=False)
print('success') 
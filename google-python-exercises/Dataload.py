# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 20:14:35 2019

@author: vibandari
"""

# import openpyxl module 
import openpyxl 
  
# Give the location of the file 
path = "C:\\Users\\vibandari\\Desktop\\New folder (3)\\AutoNumberToExtIdHIS.csv"
  
# to open the workbook  
# workbook object is created 
wb_obj = openpyxl.load_workbook(path) 
sheet_obj = wb_obj.active 
  
# print the total number of rows 
print(sheet_obj.max_row) 